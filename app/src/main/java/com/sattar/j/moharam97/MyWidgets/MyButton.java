package com.sattar.j.moharam97.MyWidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.sattar.j.moharam97.R;

public class MyButton extends AppCompatButton {

    public MyButton(Context context) {
        super(context);
        typeFont();
        setEnable();
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFont();
        setEnable();
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void typeFont() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                "font/Shabnam.ttf");
        setTypeface(face);
    }

    public void setEnable() {
        if (isEnabled()) {
            setBackground(getResources().getDrawable(R.drawable.button_enable));
            setTextColor(getResources().getColor(R.color.text_color_button_enable));
        } else {
            setBackground(getResources().getDrawable(R.drawable.button_disable));
            setTextColor(getResources().getColor(R.color.text_color_button_disable));
        }
    }

}
