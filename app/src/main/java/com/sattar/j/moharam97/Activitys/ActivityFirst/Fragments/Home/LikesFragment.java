package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sattar.j.moharam97.Contracts.FragmentsCateContract;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Adapter.TabCateNewAdapter;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Presenter;
import com.sattar.j.moharam97.Managers.DownloadManager;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.R;
import com.sattar.j.moharam97.Tools.EndlessRecyclerViewScrollListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by javid on 10/25/18.
 */

public class LikesFragment extends android.support.v4.app.Fragment implements FragmentsCateContract.View {
    public static final String CATE = "like";
    private List<MusicPojo> music = new ArrayList<>();
    private int page = 1;
    private EndlessRecyclerViewScrollListener scrollListener;
    private View view;
    private RecyclerView recNews;
    private TabCateNewAdapter adapter;
    private Presenter presenter;

    public static LikesFragment fragment;

    public static LikesFragment newInstance() {
        if (fragment == null)
            fragment = new LikesFragment();
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new Presenter();
        presenter.attachView(this);
        presenter.getItems(page, CATE);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new Presenter();
        presenter.attachView(this);
        presenter.getItems(page, CATE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_news, container, false);

        recNews = view.findViewById(R.id.recNews);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recNews.setLayoutManager(layoutManager);
        recNews.setItemAnimator(new DefaultItemAnimator());
        adapter = new TabCateNewAdapter(music, getActivity());
        recNews.setAdapter(adapter);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.getItems(page, CATE);
            }
        };
        recNews.addOnScrollListener(scrollListener);
        return view;
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        File file = new File(String.valueOf(getContext().getExternalFilesDir("avatars")));
        for (MusicPojo musicPojo : music) {
            DownloadManager.getInstance().Download(musicPojo.getAvatar(), musicPojo.getId() + ".png", String.valueOf(file), 1);
            this.music.add(musicPojo);
        }
        adapter.setList(this.music);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void musicPojoonListFailedItems(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
