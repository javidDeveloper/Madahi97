package com.sattar.j.moharam97.Managers;

import android.content.Context;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Priority;
import com.downloader.Progress;
import com.downloader.Status;
import com.sattar.j.moharam97.Contracts.DownloadManagerStatus;
import com.sattar.j.moharam97.Publics.ContextModel;
import com.sattar.j.moharam97.Tools.Utility;

import java.io.File;

/**
 * Created by javid on 12/3/18.
 */

public class DownloadManager implements DownloadManagerStatus.Manager {

    public static DownloadManager downloadManager;
    private static final String SUCCESSES_DOWNLOAD = "success.download";
    private static final String FAIL_DOWNLOAD = "fail.download";
    private static final String CANCEL_DOWNLOAD = "cancel.download";
    private DownloadManagerStatus.Requester requester;
    private static final int CONNECT_TIMEOUT = 5000;
    private String url;
    private String fileNameAndType;
    private int downloadID;
    private long progressPercent;
    private Context context;


    public static DownloadManager getInstance() {
        if (downloadManager == null) {
            downloadManager = new DownloadManager();
        }
        return downloadManager;
    }

    public DownloadManager() {
        context = ContextModel.getContext();
    }

    public int getDownloadID() {
        return downloadID;
    }

    @Override
    public void attachRequester(DownloadManagerStatus.Requester requester) {
        this.requester = requester;

    }

    @Override
    public Status getStatusDl(int Id) {
        return PRDownloader.getStatus(Id);
    }

    @Override
    public void pauseDl(int Id) {
        PRDownloader.pause(Id);
    }

    @Override
    public void resumeDl(int Id) {
        PRDownloader.resume(Id);
    }

    @Override
    public void cancelDl(int Id) {
        PRDownloader.cancel(Id);
    }

    @Override
    public void Download(String link, String nameAndType, String dirPath, int Id) {

        File directory = new File(dirPath);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().equals(nameAndType)) {
                return;
            }
        }
        this.url = link;
        this.fileNameAndType = nameAndType;
        this.downloadID = Id;
        if (Status.RUNNING == PRDownloader.getStatus(getDownloadID())) {
            PRDownloader.pause(getDownloadID());
            return;
        }
        if (Status.PAUSED == PRDownloader.getStatus(getDownloadID())) {
            PRDownloader.resume(getDownloadID());
            return;
        }
        downloadID = PRDownloader.download(url, dirPath, fileNameAndType)
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setPriority(Priority.HIGH)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
//                        requester.onFailDl(CANCEL_DOWNLOAD, downloadID);
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
//                        progressPercent = progress.currentBytes * 100 / progress.totalBytes;
//                        requester.showProgressbarDl((int) progressPercent, downloadID);
//                        requester.showTextViewProgressDl(Utility.getProgressDisplayLine(progress.currentBytes, progress.totalBytes), downloadID);
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
//                        requester.onSuccessDl(SUCCESSES_DOWNLOAD, downloadID);
                    }

                    @Override
                    public void onError(Error error) {
//                        requester.onFailDl(FAIL_DOWNLOAD, downloadID);
                    }
                });
    }

    @Override
    public void DownloadMusic(String url, String fileNameAndType, String dirPath, int Id) {
        this.url = url;
        this.fileNameAndType = fileNameAndType;
        this.downloadID = Id;
        if (Status.RUNNING == PRDownloader.getStatus(Id)) {
            PRDownloader.pause(Id);
            return;
        }
        if (Status.PAUSED == PRDownloader.getStatus(Id)) {
            PRDownloader.resume(Id);
            return;
        }
        downloadID = PRDownloader.download(url, dirPath, fileNameAndType)
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setPriority(Priority.HIGH)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        requester.onFailDl(CANCEL_DOWNLOAD, downloadID);
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                        requester.showProgressbarDl((int) progressPercent, downloadID);
                        requester.showTextViewProgressDl(Utility.getProgressDisplayLine(progress.currentBytes, progress.totalBytes), downloadID);
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        requester.onSuccessDl(SUCCESSES_DOWNLOAD, downloadID);
                    }

                    @Override
                    public void onError(Error error) {
                        requester.onFailDl(FAIL_DOWNLOAD, downloadID);
                    }
                });
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
