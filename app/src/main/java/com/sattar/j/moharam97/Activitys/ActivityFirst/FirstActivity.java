package com.sattar.j.moharam97.Activitys.ActivityFirst;

import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.marcoscg.dialogsheet.DialogSheet;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Favorite.FavoriteFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Grouping.GroupingFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.HomeFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Profile.ProfileFragment;
import com.sattar.j.moharam97.Publics.BaseActivity;
import com.sattar.j.moharam97.R;
import com.sattar.j.moharam97.Tools.Utility;


public class FirstActivity extends BaseActivity {
    private BottomBar bottomBar;
    private Fragment frament = new Fragment();
    private FragmentTransaction transaction;
    private boolean backed = true;
    private int currentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initView();
        bottomBar.selectTabAtPosition(2);
        bottomBarClick();
    }

    private void bottomBarClick() {
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                switch (tabId) {
                    case R.id.tab_exit:
                        bottomBar.selectTabWithId(currentId);
                        showExitDialog();
                        break;
                    case R.id.tab_home:
                        currentId = R.id.tab_home;
                        frament = HomeFragment.newInstance();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_continer, frament);
                        transaction.commit();
                        break;
                    case R.id.tab_tag:
                        currentId = R.id.tab_tag;
                        frament = FavoriteFragment.newInstance();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_continer, frament);
                        transaction.commit();
                        break;
                    case R.id.tab_grouping:
                        currentId = R.id.tab_grouping;
                        frament = GroupingFragment.newInstance();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_continer, frament);
                        transaction.commit();
                        break;
                    case R.id.tab_profile:
                        currentId = R.id.tab_profile;
                        frament = ProfileFragment.newInstance();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_continer, frament);
                        transaction.commit();
                        break;
                }
            }
        });
    }


    private void initView() {
        bottomBar = findViewById(R.id.bottomBar);
    }

    private void showExitDialog() {
        new DialogSheet(mContext)
                .setTitle(R.string.exit)
                .setIcon(R.mipmap.ic_launcher)
                .setMessage(R.string.exitQuestion)
                .setCancelable(true)
                .setPositiveButton(R.string.yes, new DialogSheet.OnPositiveClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.insertComment, new DialogSheet.OnNegativeClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .setBackgroundColor(Color.WHITE)
                .setButtonsColorRes(R.color.colorPrimary)
                .show();
    }

    @Override
    public void onBackPressed() {
        if (backed) {
            backed = false;
            Toast.makeText(mContext, R.string.retryPressBackToExit, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backed = true;
                }
            }, 1500);
        } else {
            super.onBackPressed();
        }

    }
}
