package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.HomeRecycelerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.MusicPlayerActivity;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;

import java.io.File;
import java.util.List;

/**
 * Created by javid on 9/29/18.
 */

public class MusicRecyclerViewAdapter extends RecyclerView.Adapter<MusicRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<MusicPojo> musicPojos;
    private View rootView;

    public MusicRecyclerViewAdapter(Context context, List<MusicPojo> musicPojos) {
        this.context = context;
        this.musicPojos = musicPojos;
    }

    public void setMusicPojos(List<MusicPojo> musicPojos) {
        this.musicPojos = musicPojos;
    }

    @Override
    public MusicRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(context).inflate(R.layout.layout_music_list_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.avatar.Load(String.valueOf(new File(context.getExternalFilesDir("avatars"),musicPojos.get(position).getId()+".png")));
        holder.artist.setText(musicPojos.get(position).getArtist());
        holder.title.setText(musicPojos.get(position).getTitle());
        holder.itemMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MusicPlayerActivity.class);
                intent.putExtra("track", musicPojos.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicPojos != null ? musicPojos.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyImageView avatar;
        MyTextView title;
        MyTextView artist;
        CardView itemMusic;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.imgAvatar);
            title = itemView.findViewById(R.id.txtTitleMusic);
            artist = itemView.findViewById(R.id.txtArtistName);
            itemMusic = itemView.findViewById(R.id.cardItemMusic);
        }
    }
}
