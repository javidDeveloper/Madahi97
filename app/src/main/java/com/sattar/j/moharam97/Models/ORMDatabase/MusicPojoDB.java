package com.sattar.j.moharam97.Models.ORMDatabase;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class MusicPojoDB extends SugarRecord {
    @Unique
    private Long id;
    private String idWeb;
    private String idUnic;
    private String cate;
    private String title;
    private String artist;
    private String avatar;
    private String link;
    private String fav;
    private String likePost;
    private String addBy;
    private String date;
    private String cat;
    private String textMusic;
    private int userFav;

    public MusicPojoDB() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdWeb() {
        return idWeb;
    }

    public void setIdWeb(String idWeb) {
        this.idWeb = idWeb;
    }

    public String getIdUnic() {
        return idUnic;
    }

    public void setIdUnic(String idUnic) {
        this.idUnic = idUnic;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }


    public String getLikePost() {
        return likePost;
    }

    public void setLikePost(String likePost) {
        this.likePost = likePost;
    }

    public String getAddBy() {
        return addBy;
    }

    public void setAddBy(String addBy) {
        this.addBy = addBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getText_music() {
        return textMusic;
    }

    public void setText_music(String text_music) {
        this.textMusic = text_music;
    }

    public String getTextMusic() {
        return textMusic;
    }

    public void setTextMusic(String textMusic) {
        this.textMusic = textMusic;
    }

    public int getUserFav() {
        return userFav;
    }

    public void setUserFav(int userFav) {
        this.userFav = userFav;
    }
}
