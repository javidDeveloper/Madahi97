package com.sattar.j.moharam97.Models.Retrofit;

import com.sattar.j.moharam97.Contracts.FirstActivityContract;
import com.sattar.j.moharam97.Contracts.FragmentsCateContract;
import com.sattar.j.moharam97.Contracts.MoreActivityContract;
import com.sattar.j.moharam97.Contracts.MusicPlayerActivityContract;
import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRest {
    private FirstActivityContract.Model firstAcModel;
    private MoreActivityContract.Model moreAcModel;
    private MusicPlayerActivityContract.Model musicPlayerAcModel;
    private FragmentsCateContract.Model fragmentCateModel;
    public List<MusicPojo> musicPojos = new ArrayList<>();
    public List<BannerPojo> bannerPojos = new ArrayList<>();

    public void attachfirstAcModel(FirstActivityContract.Model model) {
        this.firstAcModel = model;
    }

    public void attachmoreAcModel(MoreActivityContract.Model model) {
        this.moreAcModel = model;
    }

    public void attachmusicPlayerAcModel(MusicPlayerActivityContract.Model model) {
        this.musicPlayerAcModel = model;
    }

    public void attachmusicFragmentCate(FragmentsCateContract.Model model) {
        this.fragmentCateModel = model;
    }

    public void getListMusic(int page, final String cat) {

        ConnectToWebService.getClient().create(WebServices.class).getCategory(page, cat).enqueue(new Callback<List<MusicPojo>>() {
            @Override
            public void onResponse(Call<List<MusicPojo>> call, Response<List<MusicPojo>> response) {
                for (MusicPojo musicPojo : response.body()) {
                    musicPojo.setCat(cat);
                    musicPojos.add(musicPojo);
                }
                success(cat);
            }

            @Override
            public void onFailure(Call<List<MusicPojo>> call, Throwable t) {
                firstAcModel.onListFailedItems(t.getMessage());

            }
        });
    }

    public void getBanner(final int limit) {

        ConnectToWebService.getClient().create(WebServices.class).getBanner(limit).enqueue(new Callback<List<BannerPojo>>() {

            @Override
            public void onResponse(Call<List<BannerPojo>> call, Response<List<BannerPojo>> response) {
                for (BannerPojo bannerPojo : response.body()) {
                    bannerPojos.add(bannerPojo);
                }
            }

            @Override
            public void onFailure(Call<List<BannerPojo>> call, Throwable t) {
                firstAcModel.onListFailedItems(t.getMessage());

            }
        });
    }

    public void success(String cat) {

        if (musicPojos.size() == 0 || bannerPojos.size() == 0) {

        } else {
            firstAcModel.onListSuccessItems(bannerPojos, musicPojos, cat);
        }

    }

    public void getMusicMore(int page, final String cat) {
        ConnectToWebService.getClient().create(WebServices.class).getCategory(page, cat)
                .enqueue(new Callback<List<MusicPojo>>() {
                    @Override
                    public void onResponse(Call<List<MusicPojo>> call, Response<List<MusicPojo>> response) {
                        moreAcModel.onListSuccessItems(response.body(), cat);
                    }

                    @Override
                    public void onFailure(Call<List<MusicPojo>> call, Throwable t) {
                        moreAcModel.onListFailedItems(t.getMessage());
                    }
                });
    }

    public void getMusicMusicPlayer(int page, final String cat) {
        ConnectToWebService.getClient().create(WebServices.class).getCategory(page, cat)
                .enqueue(new Callback<List<MusicPojo>>() {
                    @Override
                    public void onResponse(Call<List<MusicPojo>> call, Response<List<MusicPojo>> response) {
                        musicPlayerAcModel.onListSuccessItems(response.body(), cat);
                    }

                    @Override
                    public void onFailure(Call<List<MusicPojo>> call, Throwable t) {
                        musicPlayerAcModel.onListFailedItems(t.getMessage());
                    }
                });
    }

    public void getCateMusic(int page, final String cat) {
        ConnectToWebService.getClient().create(WebServices.class).getCategory(page, cat)
                .enqueue(new Callback<List<MusicPojo>>() {
                    @Override
                    public void onResponse(Call<List<MusicPojo>> call, Response<List<MusicPojo>> response) {
                        fragmentCateModel.onListSuccessItems(response.body(), cat);
                    }

                    @Override
                    public void onFailure(Call<List<MusicPojo>> call, Throwable t) {
                        fragmentCateModel.onListFailedItems(t.getMessage());
                    }
                });
    }

    public void addView(int id) {
        ConnectToWebService.getClient().create(WebServices.class).updateView(id)
                .enqueue(new Callback<List<String>>() {
                    @Override
                    public void onResponse(Call<List<String>> call, Response<List<String>> response) {
//                        Log.d( "//onResponse: ",response.body()+"");
                    }

                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {
//                        Log.d( "//onResponse: ",t.getMessage());

                    }
                });

    }
}

