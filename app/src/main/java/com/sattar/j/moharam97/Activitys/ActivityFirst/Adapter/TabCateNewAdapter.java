package com.sattar.j.moharam97.Activitys.ActivityFirst.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.MusicPlayerActivity;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;

import java.io.File;
import java.util.List;

/**
 * Created by javid on 10/25/18.
 */

public class TabCateNewAdapter extends RecyclerView.Adapter<TabCateNewAdapter.ViewHolder> {
    List<MusicPojo> list;
    Context context;
    View view;

    public void setList(List<MusicPojo> list) {
        this.list = list;
    }

    public TabCateNewAdapter(List<MusicPojo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public TabCateNewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.layout_music_cate_item,parent,false);
        ViewHolder  holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TabCateNewAdapter.ViewHolder holder, final int position) {
        holder.avatar.Load(String.valueOf(new File(context.getExternalFilesDir("avatars"),list.get(position).getId()+".png")));
        holder.artist.setText(list.get(position).getArtist());
        holder.title.setText(list.get(position).getTitle());
        holder.txtSeenMusic.setText(list.get(position).getViewPost());
        holder.txtLikeMusic.setText(list.get(position).getLikePost());
        holder.itemMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MusicPlayerActivity.class);
                intent.putExtra("track", list.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        MyImageView avatar;
        MyTextView title;
        MyTextView artist;
        MyTextView txtLikeMusic;
        MyTextView txtSeenMusic;
        LinearLayout itemMusic;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.imgAvatar);
            title = itemView.findViewById(R.id.txtTitleMusic);
            txtLikeMusic = itemView.findViewById(R.id.txtLikeMusic);
            txtSeenMusic = itemView.findViewById(R.id.txtSeenMusic);
            artist = itemView.findViewById(R.id.txtArtistName);
            itemMusic = itemView.findViewById(R.id.itemMusic);
        }
    }
}
