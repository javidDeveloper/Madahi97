package com.sattar.j.moharam97.Models.Retrofit;


import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebServices {
///http://javiddeveloper.ir/webService/madahi/ver1/getMusic.php?page=1&cat=new
        @GET("webService/madahi/ver1/getMusic.php")
        Call<List<MusicPojo>> getCategory(@Query("page") int page
                , @Query("cat") String cat);
        //*** banner
    //** http://javiddeveloper.ir/webService/madahi/ver1/banner.php?limit=10
    //** http://www.mocky.io/v2/5bb3d3643300005c00cad455
        @GET("webService/madahi/ver1/banner.php")
        Call<List<BannerPojo>> getBanner(@Query("limit") int limit);

    //** http://javiddeveloper.ir/webService/madahi/ver1/addSeen.php?id=1
        @GET("webService/madahi/ver1/addSeen.php")
        Call<List<String>> updateView (@Query("id") int limit);

}

