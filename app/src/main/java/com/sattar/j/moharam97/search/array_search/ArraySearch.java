package com.sattar.j.moharam97.search.array_search;

import android.util.Pair;


import com.sattar.j.moharam97.Publics.ContextModel;
import com.sattar.j.moharam97.Tools.Utility;
import com.sattar.j.moharam97.search.IndexinStatus;
import com.sattar.j.moharam97.search.OnFinishIndexing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by mansour on 2/18/18.
 */

public class ArraySearch<T extends Searchable> {
    private List<T> items;
    private List<Pair<String, List<Integer>>> words;
    private List<Pair<Integer, List<String>>> indexWords;
    private List<Pair<String, List<Integer>>> currentWords;
    private List<T> currentSearchList;
    private String searchWord;
    private List<String> searchWords;
    private String previousWord;
    private String[] separators = new String[]{" ", ",", "-", "", "/", "\\", "(", ")", ".", "@", ":", "،"};

    private IndexinStatus indexinStatus;

    public ArraySearch() {
        indexinStatus = IndexinStatus.NotIndexed;
    }

    private List<String> separateWords(String words) {
        if (words == null) {
            return new ArrayList<>();
        }
        String separator = " ,-/\\().@:،";
        List<String> resultTemp = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(words, separator, false);
        while (tokenizer.hasMoreTokens()) {
            resultTemp.add(tokenizer.nextToken());
        }
        List<String> result = new ArrayList<>();


        for (int i = 0; i < resultTemp.size(); i++) {
            String word = Utility.convertPersianCharacters(resultTemp.get(i));
            if (word != null && word.length() > 0) {
                result.add(word);
            }
        }

        return result;
    }


    private void resetCurrentSearchList() {
        if (currentSearchList == null) {
            currentSearchList = Collections.synchronizedList(new ArrayList<T>());
        } else {
            currentSearchList.clear();
            currentWords = new ArrayList<>();
            if (items != null)
                currentSearchList.addAll(items);
        }
    }

    private void updateSearch() {
        if (searchWord == null || searchWord.equals("") || items == null || items.size() == 0) {
            resetCurrentSearchList();
            return;
        }

        //First step: Find compatible words in list and their indexes
        int searchIndex = 0;
        for (String searchItem : searchWords) {
            Set<Integer> indexSet = new HashSet<>();
            if (currentWords != null &&
                    previousWord != null && !previousWord.equals("") &&
                    (searchIndex > 0 || searchWord.contains(previousWord))) {// previous word added by a character or second search word is the current word, So search in current word list
                for (Pair<String, List<Integer>> item : currentWords) {
                    if (item.first.startsWith(searchItem) || item.first.equals(searchItem)) {
                        for (Integer index : item.second) {
                            indexSet.add(index);
                        }
                    }
                }
            } else if (words != null) {
                for (Pair<String, List<Integer>> item : words) {
                    if (item.first.startsWith(searchItem) || item.first.equals(searchItem)) {
                        for (Integer index : item.second) {
                            indexSet.add(index);
                        }
                    }
                }
            }
            currentWords = new ArrayList<>();
            Map<String, Integer> wordsIndex = new HashMap<>();
            for (Integer index : indexSet) { //Add all words in searched indexes to currentWords array to search next separated word
                List<String> expression = indexWords.get(index).second;
                for (String word : expression) {
                    if (wordsIndex.containsKey(word)) {
                        int i = wordsIndex.get(word);
                        currentWords.get(i).second.add(index);
                    } else {
                        wordsIndex.put(word, currentWords.size());
                        List<Integer> tempList = new ArrayList<>();
                        tempList.add(index);
                        currentWords.add(new Pair<String, List<Integer>>(word, tempList));
                    }
                }
            }
            searchIndex++;
        }
        if (currentSearchList == null) {
            currentSearchList = Collections.synchronizedList(new ArrayList<T>());
        }
        currentSearchList.clear();
        if (currentWords.size() == 0) {
            return;
        }

        Set<Integer> indexSet = new TreeSet<>();

        for (Pair<String, List<Integer>> item : currentWords) {
            for (int index : item.second) {
                indexSet.add(index);
            }
        }

        //Now bring items with more priority to top of the list

        Set<Integer> movedIndexes = new HashSet<>();
        for (int index : indexSet) {
            for (String searchItem : searchWords) {
                boolean move = false;
                for (String priorItem : items.get(index).getSortPriorities()) {
                    if (priorItem.contains(searchItem)) {
                        currentSearchList.add(items.get(index));
                        movedIndexes.add(index);//indexSet.remove(index);
                        move = true;
                        break;
                    }
                }
                if (move) {
                    break;
                }
            }
        }
        indexSet.removeAll(movedIndexes);

        Iterator<Integer> iterator = indexSet.iterator();
        while (iterator.hasNext()) {
            int index = iterator.next();
            currentSearchList.add(items.get(index));
        }

    }

    public String getCurrentSearch() {
        return searchWord;
    }

    public void setCurrentSearchText(String currentSearch) {
        previousWord = searchWord;
        searchWord = Utility.convertPersianCharacters(currentSearch != null ? currentSearch : "");
        searchWords = separateWords(searchWord);

        //refresh the current searchList
        updateSearch();
    }

    public List<T> getSearchedItems() {
        if (currentSearchList == null) {
            currentSearchList = Collections.synchronizedList(new ArrayList<T>());
        }
        return currentSearchList;
    }

    public List<T> getAllItems() {
        return items;
    }

    public synchronized void setItems(final List<T> items, final OnFinishIndexing onFinishIndexing) {
        if (items == null) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                waitThreadForFinishIndexing();  // This line keeps thread in a loop while previous indexing finish
                synchronized (this) {
                    indexinStatus = IndexinStatus.Indexing;
                    ArraySearch.this.items = new CopyOnWriteArrayList<>(items);
                    words = Collections.synchronizedList(new ArrayList<Pair<String, List<Integer>>>());
                    Map<String, Integer> wordsIndex = new HashMap<>();
                    indexWords = Collections.synchronizedList(new ArrayList<Pair<Integer, List<String>>>());
                    int i = 0;
                    for (T item : ArraySearch.this.items) {
                        if (item == null)
                            continue;
                        List<String> expression = separateWords(item.getSearchableContent());
                        for (String word : expression) {
                            if (wordsIndex.containsKey(word) && wordsIndex.get(word) < words.size()) {
                                int index = wordsIndex.get(word);
                                try {
                                    if (index < words.size() && words.get(index) != null)
                                        words.get(index).second.add(i);
                                } catch (Exception e) {
//                                    Crashlytics.logException(e);
                                }
                            } else {
                                wordsIndex.put(word, words.size());
                                List<Integer> tempList = new ArrayList<>();
                                tempList.add(i);
                                words.add(new Pair<>(word, tempList));
                            }
                        }
                        indexWords.add(new Pair<Integer, List<String>>(i, expression));
                        i++;
                    }

                    //sort words according to words ascending
                /*Collections.sort(words, new Comparator<Pair<String, List<Integer>>>() {
                    @Override
                    public int compare(Pair<String, List<Integer>> stringListPair, Pair<String, List<Integer>> t1) {
                        return stringListPair.first.compareTo(t1.first);
                    }
                });*/  //java.lang.IllegalArgumentException: Comparison method violates its general contract!

                    updateSearch();

                    indexinStatus = IndexinStatus.Indexed;
                    if (onFinishIndexing != null && ContextModel.getCurrentActivity() != null) {
                        /*Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                onFinishIndexing.onFinish();
                            }
                        });*/
                        ContextModel.getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onFinishIndexing.onFinish();
                            }
                        });
                    }
                }
            }
        }, "ArraySearchSetAndIndexItems->").start();

    }

    private void waitThreadForFinishIndexing() {
        int i = 0;
        while (indexinStatus == IndexinStatus.Indexing && i < 100) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            i++;
        }
    }

    public IndexinStatus getIndexinStatus() {
        return indexinStatus;
    }
}
