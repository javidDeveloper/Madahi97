package com.sattar.j.moharam97.Contracts;

import android.app.Fragment;
import android.view.View;

import com.downloader.PRDownloader;
import com.downloader.Status;

/**
 * Created by javid on 12/4/18.
 */

public interface DownloadManagerStatus {
    interface Requester {
        void onSuccessDl(String success, int id);
        void onFailDl(String error, int id);
        void showProgressbarDl(int progressPercent, int id);
        void showTextViewProgressDl(String progressPercent, int id);
    }

    interface Manager {
        void attachRequester(Requester requester);

        void Download(String url, String fileNameAndType,String dirPath, int Id);
        void DownloadMusic(String url, String fileNameAndType,String dirPath, int Id);

        Status getStatusDl(int Id);

        void pauseDl(int Id);

        void resumeDl(int Id);

        void cancelDl(int Id);
    }

}
