package com.sattar.j.moharam97.Tools;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sattar.j.moharam97.Publics.ContextModel;
import com.sattar.j.moharam97.R;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Created by javid on 10/18/18.
 */

public class Utility {

    public static String setMusicFullText(String fullText) {
        String text="";
String limText = "";
        if (fullText.length() > 50) {
            String[] s = fullText.split("");
            for (int i = 0; i < s.length; i++) {
                text += s[i];
                if (i == 50) {
                    limText = text +"...";
                }
            }
        }
        return limText;
    }

    public static void goToNextActivity(Context context, Class goTo) {
        Intent intent = new Intent(context, goTo);
        context.startActivity(intent);
    }

    public static void goToActivityWithKey(Context context, Class goTo, String valueOfKey) {
        Intent intent = new Intent(context, goTo);
        intent.putExtra("key", valueOfKey);
        context.startActivity(intent);
    }

    public static int SECTION_NUMBER_SETTING_FRAGMENT = 12;
    public static final int REQUEST_READ_PHONE_STATE = 100;
    //    public static String WORDS_DELIMITERS = " .,-/،()";
    public static int BLINK_TEXT_TIME = 10000; // Blink time in millisecond
    public static int NORMAL_TEXT_COLOR = Color.parseColor("#FF777777");
    public static int GREEN_TEXT_COLOR = Color.parseColor("#FF77FF77");
    public static int RED_TEXT_COLOR = Color.parseColor("#FFFF7777");
    //    public static String FONT_NAME = "BYekan";
    public final static String PORTFOLIO_FRAGMENT = "portfolio.fragment";
    public final static String WATCH_FRAGMENT = "watch.fragment";

    public static String USER_NAME_KEY = "user.name.key";
    public static String PASSWORD_KEY = "password.key";

    public static String FONT_NAME = "Shabnam-FD";

    //    public static Boolean IS_TABLET = ContextModel.getContext().getResources().getBoolean(R.bool.isTablet);
    public static int SETTING_FRAGMNET_INDEX;


    public static Typeface getApplicationTypeface(String textStyle) {
        try {
            if (textStyle == null) {
                return Typeface.createFromAsset(ContextModel.getContext().getAssets(), "fonts/" + FONT_NAME + ".ttf");
            } else if (textStyle.equals("bold")) {
                return Typeface.createFromAsset(ContextModel.getContext().getAssets(), "fonts/" + FONT_NAME + "-Bold.ttf");
            } else if (textStyle.equals("light")) {
                return Typeface.createFromAsset(ContextModel.getContext().getAssets(), "fonts/" + FONT_NAME + "-Light.ttf");
            } else {
                return Typeface.createFromAsset(ContextModel.getContext().getAssets(), "fonts/" + FONT_NAME + ".ttf");
            }
        } catch (Exception e) {
            return null;
        }
    }


    public static String convertNumbersToPersian(String text) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (((int) c) >= 48 && ((int) c) < 58) {
                c = (char) ((c - '0') + 0x0660);
            }
            result.append(c);
        }
        return result.toString();
    }

    public static void asyncRunAfterSleep(final int millis, final Runnable runnable) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (runnable != null) {
                    runnable.run();
                }
            }
        }, "Delayed Run Thread " + millis + " millis").start();

    }

    public static void mainThreadRunAfterSleep(final Activity activity, final int millis, final Runnable runnable) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (runnable != null && activity != null) {
                    activity.runOnUiThread(runnable);
                }
            }
        }, "Delayed Run Thread " + millis + " millis").start();

    }

    public static void delayedRun(int millis, Runnable runnable) {
        delayedRun(millis, true, runnable);
    }

    public static void delayedRun(int millis, boolean mainLooper, Runnable runnable) {
        if (runnable == null)
            return;
        Handler handler;
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (mainLooper)
            handler = new Handler(Looper.getMainLooper());
        else
            handler = new Handler();
//        handler.getLooper().prepare();
        handler.postDelayed(runnable, millis);
//        handler.post(runnable);
    }

    public static boolean isAppRunning(Context context) {
        String packageName = "com.rh.ot.android";

        return isAppRunning(context, packageName);

    }

    public static boolean isAppRunning(Context context, String packageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        boolean isActivityFound = false;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningTaskInfo> services = activityManager
                    .getRunningTasks(Integer.MAX_VALUE);
            if (services.get(0).topActivity.getPackageName().toString()
                    .equalsIgnoreCase(/*context.getPackageName().toString()*/packageName)) {
                isActivityFound = true;
            }
        } else {
            String[] activePackages = getActivePackages(activityManager);
            isActivityFound = (activePackages != null && activePackages.length > 0 && activePackages[0].equals(packageName));
            for (int i = 0; i < activePackages.length; i++) {
                Log.e("Running App " + i + " :", activePackages[i]);
            }
        }

        return isActivityFound;

    }

    public static String[] getActivePackages(ActivityManager activityManager) {
        final List<String> activePackages = new ArrayList<>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager.getRunningAppProcesses();
        if (processInfos == null) {
            return new String[]{};
        }
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(Arrays.asList(processInfo.pkgList));
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static PackageInfo getPackageInfo(String packagename, PackageManager packageManager) {
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packagename, 0);
            return packageInfo;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

//    public static boolean killAppActivity(Context context) {
//        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> services = activityManager
//                .getRunningTasks(Integer.MAX_VALUE);
//        for(int i=0; i<services.size(); i++){
//            if (services.get(i).topActivity.getPackageName().toString()
//                    .equalsIgnoreCase(context.getPackageName().toString())) {
//                services.remove(i);
//                return true;
//            }
//        }
//        return false;
//    }

    public static String getCurrentTime(boolean rtl) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int hour = new Time(System.currentTimeMillis()).getHours();
        int minute = new Time(System.currentTimeMillis()).getMinutes();
        String H = hour < 10 ? "0" : "" + hour;
        String M = minute < 10 ? "0" : "" + minute;
        if (rtl)
            return M + ":" + H;
        else
            return H + ":" + M;
    }

    public static String convertPersianCharacters(String word) {
        word = word.replace((char) 1610, (char) 1740);
        word = word.replace((char) 0x643, (char) 0x6a9);
        return word;
    }

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    public static Point getWindowSize() {
        WindowManager wm = (WindowManager) ContextModel.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
        return size;
    }

    public static int getWindowHeight() {
        return getWindowSize().y;
    }

    public static int getWindowWidth() {
        return getWindowSize().x;
    }

    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = ContextModel.getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px) {
        DisplayMetrics displayMetrics = ContextModel.getContext().getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static float pxToSp(float px) {
        float scaledDensity = ContextModel.getContext().getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static float spToPx(float sp) {
        float scaledDensity = ContextModel.getContext().getResources().getDisplayMetrics().scaledDensity;
        return sp * scaledDensity;
    }

    public static String formatNumbers(double number) {
        String s;
        int fPoint; //position of floating in number string
        if (isInteger(number)) {
            s = (long) number + "";
            fPoint = s.length();
        } else {
            s = number + "";
            fPoint = s.lastIndexOf('.');
        }
        StringBuilder formatted = new StringBuilder(s);
        for (int i = fPoint - 3; i > 0; i -= 3) {
            formatted.insert(i, ',');
        }

        return formatted.toString();
    }

    public static String formatNumbers(String number) {
        if (number == null || "".equals(number))
            return "";
        if (!isFloatText(number))
            return number;
        String s = number;

        int fPoint = number.lastIndexOf('.'); //position of floating in number string
        try {
            int f = 0;
            if (!number.contains(".")) {
                fPoint = number.length();
                f = 0;
            } else if (fPoint < number.length() - 1) {
                f = Integer.parseInt(number.substring(fPoint + 1));
            }

            if (f == 0) {
                s = number.substring(0, fPoint);
            }
        } catch (Exception e) {
        }
        StringBuilder formatted = new StringBuilder(s);
        for (int i = fPoint - 3; i > 0; i -= 3) {
            if (i != 1 || number.charAt(0) != '-')
                formatted.insert(i, ',');
        }

        return formatted.toString();
    }

    public static boolean isFloatText(String text) {
        try {
            Float.parseFloat(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInteger(double number) {
        return (Math.floor(number)) == number;
    }

    public static void schedule(final Runnable runnable, long period, int timeMillis) {
        repeatTask(runnable, period, 0, (int) (timeMillis / period));
    }

    private static void repeatTask(final Runnable runnable, final long period, final int repeated, final int repeat) {
        if (repeated >= repeat)
            return;
        runnable.run();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                repeatTask(runnable, period, repeated + 1, repeat);
            }
        }, period);
    }

    public static int getGreenColor(float multiplier) {
        int power = (int) ((0xff - 0x77) * multiplier) + 0x77;
        return Color.parseColor("#FF77" + toHexString(power) + "77");

    }

    public static int getRedColor(float multiplier) {
        int power = (int) ((0xff - 0x77) * multiplier) + 0x77;
        return Color.parseColor("#FF" + toHexString(power) + "7777");

    }

    public static int increaseColor(int color, float multiplier) {
        Log.e("INITIAL COLOR", String.format("%X", color));


        int a = (color & 0xFF000000) >> 24;// Integer.parseInt(alpha, 16);
        int r = (color & 0x00FF0000) >> 16;//Integer.parseInt(red, 16);
        int g = (color & 0x0000FF00) >> 8;//Integer.parseInt(green, 16);
        int b = (color & 0x000000FF);//Integer.parseInt(blue, 16);
        r = (int) Math.min(255, (int) (multiplier * r));
        g = (int) Math.min(255, (int) (multiplier * g));
        b = (int) Math.min(255, (int) (multiplier * b));
        Log.e("ALPHA", String.format("%X", a));
        Log.e("red", String.format("%X", r));
        Log.e("green", String.format("%X", g));
        Log.e("blue", String.format("%X", b));

        int nColor = (a << 24) | (r << 16) | (g << 8) | b;//Integer.parseInt(String.format("%X%X%X%X",a, r, g, b), 16);//(int) (b + g * Math.pow(16,2) + r * Math.pow(16, 4) + a * Math.pow(16, 6));

        Log.e("INCREASE COLOR TO", String.format("%X", nColor));

        return nColor;

    }

    public static SpannableString paintString(String text, int period, int color1, int color2, boolean rtl) {
        SpannableString s = new SpannableString(text);
        if (period == 0) {
            s.setSpan(new BackgroundColorSpan(color1), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            if (rtl) {
                for (int i = text.length(); i >= 0; i -= period) {
                    int to = i;
                    int from = Math.max(0, i - period);
                    s.setSpan(new BackgroundColorSpan((text.length() - i) / period % 2 == 0 ? color1 : color2), from, to, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } else {
                for (int i = 0; i < text.length(); i += period) {
                    int to = Math.min(0, i + period);
                    int from = i;
                    s.setSpan(new BackgroundColorSpan(i / period % 2 == 0 ? color1 : color2), from, to, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return s;
    }

    public static String toHexString(int number) {
        return String.format("%02X", number & 0xFF);
    }


    public static void saveString(String key, String value) {
        SharedPreferences preferences = ContextModel.getContext().getSharedPreferences("Utility", 0);
        preferences.edit().putString(key, value).commit();
    }

    public static String getSavedString(String key) {
        SharedPreferences preferences = ContextModel.getContext().getSharedPreferences("Utility", 0);
        return preferences.getString(key, "");
    }

//    public static SpannableString formatStringFont(String text) {
//        SpannableString s = new SpannableString(text);
//        s.setSpan(new CustomTypefaceSpan("", getApplicationTypeface(null)), 0, s.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        return s;
//    }

    public static String convertFloatToTimeString(float timeFloat) {
        int hour = (int) timeFloat;
        int minute = Math.round((timeFloat - hour) * 60);
        String time = (hour > 9 ? "" : "0") + hour;
        time += ":" + (minute > 9 ? "" : "0") + minute;

        return time;
    }

    public static void setListViewHeightWrapContent(ListView listView) {
        BaseAdapter adapter = (BaseAdapter) listView.getAdapter();
        int height = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, null);
            listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            height += listItem.getMeasuredHeight();
        }
        if (height == 0)
            height = 100;
//        height = Math.min(height, Utility.getWindowHeight() - Utility.dpToPx(200));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, height);
//        params.setMargins(Utility.dpToPx(10), 0, Utility.dpToPx(10), Utility.dpToPx(10));
        listView.setLayoutParams(params);
    }

    public static String getDeviceId(Context context) {
        String id = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        //TODO set permission
        try {
            id = telephonyManager.getDeviceId();
        } catch (java.lang.SecurityException e) {
        }

        if (id != null && !"".equals(id))
            return id;
        id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (id != null && !"".equals(id))
            return id;
        WifiManager m_wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        id = m_wm.getConnectionInfo().getMacAddress();
        if (id != null && !"".equals(id))
            return id;
        SharedPreferences preferences = context.getSharedPreferences("Utility", Context.MODE_PRIVATE);
        id = preferences.getString("device_id", "");
        if (id != null && !"".equals(id))
            return id;
        id = UUID.randomUUID().toString();
        preferences.edit().putString("device_id", id).commit();
        return id;
    }


    public static String Text2htmlString(String text, int fsize) {
        String s = text.replaceAll("\\n", "<br>");
        s = s.replaceAll("%", "&#37;");
        if (!s.equals("")) {
            s = "<html><style type='text/css'></style>" +
                    "<body style='text-align:justify'> <font color='black' size=" + fsize + " > " +
                    s + "</font></body></html>";
        }
        return s;
    }

    public static void screenSize(Activity context) {
        String size;
        //Determine screen size
        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            size = "Large screen, resolution: " + resolutionOf(context);
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            size = "Normal sized screen, resolution: " + resolutionOf(context);
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            size = "Small sized screen, resolution: " + resolutionOf(context);
        } else {
            size = "Screen size is neither large, normal or small, resolution: " + resolutionOf(context);

        }
//        Toast.makeText(context, size, Toast.LENGTH_LONG).show();
        Log.v("Size", size);
    }

    public static String resolutionOf(Activity context) {
        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_XXXHIGH) {
            return "DENSITY_XXXHIGH... Density is " + String.valueOf(density);
        } else if (density == DisplayMetrics.DENSITY_XXHIGH) {
            return "DENSITY_XXHIGH... Density is " + String.valueOf(density);
        } else if (density == DisplayMetrics.DENSITY_XHIGH) {
            return "DENSITY_XHIGH... Density is " + String.valueOf(density);
        } else if (density == DisplayMetrics.DENSITY_HIGH) {
            return "DENSITY_HIGH... Density is " + String.valueOf(density);
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            return "DENSITY_MEDIUM... Density is " + String.valueOf(density);
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            return "DENSITY_LOW... Density is " + String.valueOf(density);
        } else {
            return "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density);
        }
    }

    public static void textJustification(WebView webView, String text) {
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollbarFadingEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:(function() { "
                        + "document.getElementsByTagName('body')[0].style.fontFamily = 'SHABNAM';"
                        + "document.getElementsByTagName('body')[0].style.direction = 'rtl';"
                        + "document.getElementsByTagName('body')[0].style.textAlign = 'justify';"
                        + "document.getElementsByTagName('body')[0].style.lineHeight = '1.0';"
                        + "})()");
            }
        });

        webView.loadDataWithBaseURL("file:///android_asset/", "<head><link rel='stylesheet' type='text/css' href='file:///android_asset/font.css'/></head>" + text, "text/html; charset=UTF-8", null, null);
    }

    public static void textJustificationDetails(WebView webView, String text) {
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollbarFadingEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:(function() { "
                        + "document.getElementsByTagName('body')[0].style.fontFamily = 'SHABNAM';"
                        + "document.getElementsByTagName('body')[0].style.direction = 'rtl';"
                        + "document.getElementsByTagName('body')[0].style.textAlign = 'justify';"
                        + "document.getElementsByTagName('body')[0].style.lineHeight = '1.5';"
                        + "})()");
            }
        });

        webView.loadDataWithBaseURL("file:///android_asset/", "<head><link rel='stylesheet' type='text/css' href='file:///android_asset/font.css'/></head>" + text, "text/html; charset=UTF-8", null, null);
    }


    public static void hideKeyboard(FragmentActivity activity) {
        try {
            View viewFocused = activity.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (viewFocused != null)
                imm.hideSoftInputFromWindow(viewFocused.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static int getStringIdentifierDrawableId(Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    public static void showSnackBar(CoordinatorLayout coordinatorLayout, String text) {
        if (coordinatorLayout != null) {

            Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(ContextModel.getContext().getResources().getColor(R.color.Viewline));
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(ContextModel.getContext().getResources().getColor(R.color.black));
            textView.setTypeface(Utility.getApplicationTypeface("bold"));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            else
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
            snackbar.show();


//            CustomSnackbar snackbar = CustomSnackbar.Builder(ContextModel.getContext())
//                    .layout(R.layout.my_snackbar)
//                    .background(R.color.colorAccent)
//                    .duration(CustomSnackbar.LENGTH.INDEFINITE)
//                    .swipe(false)
//                    .build(coordinatorLayout);
//            snackbar.show();
//
//            TextView textView = (TextView) snackbar.getContentView().findViewById(R.id.textView_snackbar);
//            textView.setText(text);
//            textView.setTextColor(ContextCompat.getColor(ContextModel.getContext(), R.color.color_text_white));


//            Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
//            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
//            layout.getHeight();
//            View sbView = snackbar.getView();
//            sbView.setBackgroundColor(ContextModel.getContext().getResources().getColor(R.color.color_accent));
//            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//            textView.setVisibility(View.INVISIBLE);
//            View snackView = LayoutInflater.from(ContextModel.getContext()).inflate(R.layout.my_snackbar, null);
//            TextView textViewSnackbar = (TextView) snackView.findViewById(R.id.textView_snackbar);
//            textViewSnackbar.setText(text);
//            textViewSnackbar.setTextColor(ContextModel.getContext().getResources().getColor(R.color.color_text_white));
//            textViewSnackbar.setTypeface(Utility.getApplicationTypeface("bold"));
//            textViewSnackbar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
//            textViewSnackbar.getHeight();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
//                textViewSnackbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//            else
//                textViewSnackbar.setGravity(Gravity.CENTER);
//            layout.setPadding(0,0,0,0);
//            snackView.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//            layout.addView(snackView, 0);
//            snackView.getHeight();
//            snackbar.show();
        }
    }

//    public static void changeTextStylesInSortMode(List<TextView> textViewsList, TextView textViewException) {
//        if (textViewException != null) {
//            for (TextView textView : textViewsList) {
//                if (textView != textViewException) {
//                    textView.setTypeface(Utility.getApplicationTypeface(""));
//                    textView.setTextColor(ContextModel.getContext().getResources().getColor(R.color.color_text_secondary));
//                }
//            }
//            textViewException.setTypeface(Utility.getApplicationTypeface("bold"));
//            textViewException.setTextColor(ContextModel.getContext().getResources().getColor(R.color.color_text_primary));
//        }
//    }

    public static void hideSortIcon(List<ImageView> imageViewList, ImageView exceptionalImageView) {
        if (exceptionalImageView != null) {
            for (int i = 0; i < imageViewList.size(); i++) {
                if (imageViewList.get(i) != exceptionalImageView)
                    imageViewList.get(i).setVisibility(View.GONE);
            }
            exceptionalImageView.setVisibility(View.VISIBLE);
        }
    }

    public static Boolean resetFlags(List<Boolean> flagList, Boolean exceptionalFlag) {
        for (Boolean flag : flagList)
            if (flag != exceptionalFlag)
                flag = true;
        return !exceptionalFlag;
    }

    public static void resetTextViewStyle(List<TextView> headerList, List<TextView> subList) {
        for (int i = 0; i < headerList.size(); i++) {
            headerList.get(i).setTypeface(getApplicationTypeface("bold"));
            headerList.get(i).setTextColor(ContextModel.getContext().getResources().getColor(R.color.colorPrimary));
        }
        for (int j = 0; j < subList.size(); j++) {
            subList.get(j).setTypeface(getApplicationTypeface(""));
        }
    }

    public static void hideImageViews(List<ImageView> imageViewList) {
        for (ImageView imageView : imageViewList)
            imageView.setVisibility(View.GONE);
    }

//    public static void showGuideDialog(final String fragmentType) {
//        /*Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                final Dialog guideDialog = new Dialog(ContextModel.getCurrentActivity());
//                guideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                guideDialog.setContentView(R.layout.dialog_guide);
//                guideDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//
//                TextView textViewConfirm = (TextView) guideDialog.findViewById(R.id.textView_confirm);
//                TextView textViewRemindLater = (TextView) guideDialog.findViewById(R.id.textView_remind_later);
//
//                switch (fragmentType) {
//                    case PORTFOLIO_FRAGMENT:
//                        textViewConfirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setPortfolioDialogReminder(true);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        textViewRemindLater.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setPortfolioDialogReminder(false);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        if (SettingsManager.getInstance().getPortfolioDialogReminder())
//                            guideDialog.dismiss();
//                        else
//                            guideDialog.show();
//
//                        break;
//                    case WATCH_FRAGMENT:
//                        textViewConfirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setWatchDialogReminder(true);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        textViewRemindLater.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setWatchDialogReminder(false);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        if (SettingsManager.getInstance().getWatchDialogReminder())
//                            guideDialog.dismiss();
//                        else
//                            guideDialog.show();
//
//                        break;
//                }
//            }
//        });*/
//        ContextModel.getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                final Dialog guideDialog = new Dialog(ContextModel.getCurrentActivity());
//                guideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                guideDialog.setContentView(R.layout.dialog_guide);
//                guideDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//
//                TextView textViewConfirm = (TextView) guideDialog.findViewById(R.id.textView_confirm);
//                TextView textViewRemindLater = (TextView) guideDialog.findViewById(R.id.textView_remind_later);
//
//                switch (fragmentType) {
//                    case PORTFOLIO_FRAGMENT:
//                        textViewConfirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setPortfolioDialogReminder(true);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        textViewRemindLater.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setPortfolioDialogReminder(false);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        if (SettingsManager.getInstance().getPortfolioDialogReminder())
//                            guideDialog.dismiss();
//                        else
//                            guideDialog.show();
//
//                        break;
//                    case WATCH_FRAGMENT:
//                        textViewConfirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setWatchDialogReminder(true);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        textViewRemindLater.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                SettingsManager.getInstance().setWatchDialogReminder(false);
//                                guideDialog.dismiss();
//                            }
//                        });
//
//                        if (SettingsManager.getInstance().getWatchDialogReminder())
//                            guideDialog.dismiss();
//                        else
//                            guideDialog.show();
//
//                        break;
//                }
//            }
//        });
//
//    }


    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = ContextModel.getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = ContextModel.getContext().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getAttrActionBarHeigth() {
        int actionBarHeight = 0;
        TypedValue typedValue = new TypedValue();
        if (ContextModel.getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(typedValue.data, ContextModel.getContext().getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public static void putToolbarBelowStatusBar(Toolbar toolbar) {
        AppBarLayout.LayoutParams layoutParams = new AppBarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getAttrActionBarHeigth());
        layoutParams.setMargins(0, getStatusBarHeight(), 0, 0);
        toolbar.setLayoutParams(layoutParams);
    }

//    public static void changeColorOfStatusBar() {
//        // create our manager instance after the content view is set
//        SystemBarTintManager tintManager = new SystemBarTintManager(ContextModel.getCurrentActivity());
//        // enable status bar tint
//        tintManager.setStatusBarTintEnabled(true);
//        // enable navigation bar tint
//        tintManager.setNavigationBarTintEnabled(true);
//        // set the transparent color of the status bar, 20% darker
//        tintManager.setTintColor(Color.parseColor("#20000000"));
//    }

    public static boolean isLandscape() {
        if (ContextModel.getCurrentActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return true;
        else
            return false;
    }

    public static void unbindDrawables(View view) {

        /*if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
              //  Log.i("unbindDrawables","####"+i+"removed");
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }

        System.gc();*/
    }

    public static boolean checkIfValueCouldBeInteger(String s) {
        if (s == null) {
            return false;
        }
        boolean isInteger = false;
        try {
            s = s.replaceAll(":", "");
            Integer.parseInt(s);
            //i is a valid integer
            isInteger = true;
        } catch (NumberFormatException e) {
            //not an integer
            Log.e("NumberFormatException", e.toString());
        }
        return isInteger;
    }

    public static List<String> separateWords(String words) {
        if (words == null) {
            return new ArrayList<>();
        }
        String separator = " ,-/\\().@:،";
        List<String> resultTemp = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(words, separator, false);
        while (tokenizer.hasMoreTokens()) {
            resultTemp.add(tokenizer.nextToken());
        }
        List<String> result = new ArrayList<>();


        for (int i = 0; i < resultTemp.size(); i++) {
            String word = Utility.convertPersianCharacters(resultTemp.get(i));
            if (word != null && word.length() > 0) {
                result.add(word);
            }
        }

        return result;
    }
    public static String getProgressDisplayLine(long currentBytes, long totalBytes) {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes);
    }

    private static String getBytesToMBString(long bytes){
        return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00));
    }
}
