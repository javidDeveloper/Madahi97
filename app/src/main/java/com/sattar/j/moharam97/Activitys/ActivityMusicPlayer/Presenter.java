package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer;
import com.sattar.j.moharam97.Contracts.MusicPlayerActivityContract;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

public class Presenter implements MusicPlayerActivityContract.Presenter {
    private Model model = new Model();
    private MusicPlayerActivityContract.View view;


    public Presenter() {
        model.attachPresenter(this);
    }


    @Override
    public void attachView(MusicPlayerActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        model.getItems(musicPage, musicCat);
    }

    @Override
    public void addview(int id) {
        model.addview(id);
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        view.onListSuccessItems(music, cat);
    }

    @Override
    public void onListFailedItems(String msg) {
        view.musicPojoonListFailedItems(msg);
    }

}


