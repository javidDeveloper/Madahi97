package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Favorite;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Favorite.RecyclerAdapter.FavoriteRecyclerViewAdapter;
import com.sattar.j.moharam97.Managers.DatabaseManager;
import com.sattar.j.moharam97.Models.ORMDatabase.MusicPojoDB;
import com.sattar.j.moharam97.R;

import java.util.List;

/**
 * Created by javid on 11/15/18.
 */
public class FavoriteFragment extends Fragment {

    public static FavoriteFragment fragment;
    private View rootView;
    private RecyclerView recyclerFavourite;
    private List<MusicPojoDB> music;
    private FavoriteRecyclerViewAdapter adapter;

    public static FavoriteFragment newInstance() {
        if (fragment == null)
            fragment = new FavoriteFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favorite, container, false);
        initView(rootView);
        music = DatabaseManager.getInstance().readListMusicFromDB();
        adapter = new FavoriteRecyclerViewAdapter(getActivity(), music);
        recyclerFavourite.setAdapter(adapter);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerFavourite.setLayoutManager(layoutManager);
        recyclerFavourite.setItemAnimator(new DefaultItemAnimator());
        return rootView;
    }

    private void initView(View rootView) {
        recyclerFavourite = rootView.findViewById(R.id.recycler_favourite);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        music = DatabaseManager.getInstance().readListMusicFromDB();
    }


}
