package com.sattar.j.moharam97.Contracts;

import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

public interface FirstActivityContract {
    interface View {
        void onListSuccessItems(List<BannerPojo> bannerPojos, List<MusicPojo> music, String cat);
        void onListFailedItems(String msg);
    }

    interface Presenter {
        void attachView(View view);
        void getItems(int musicPage, String musicCat);
        void getHeaders(int bannerLimit);
        void onListSuccessItems(List<BannerPojo> bannerPojos,List<MusicPojo> music,String cat);
        void onListFailedItems(String msg);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void getItems(int musicPage, String musicCat);
        void getHeaders(int bannerLimit);
        void onListSuccessItems(List<BannerPojo> bannerPojos,List<MusicPojo> music,String cat);
        void onListFailedItems(String msg);
    }
}
