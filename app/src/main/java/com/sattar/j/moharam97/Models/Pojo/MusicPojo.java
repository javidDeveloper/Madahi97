package com.sattar.j.moharam97.Models.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MusicPojo implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_unic")
    @Expose
    private String idUnic;
    @SerializedName("cate")
    @Expose
    private String cate;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("artist")
    @Expose
    private String artist;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("fav")
    @Expose
    private String fav;
    @SerializedName("view_post")
    @Expose
    private String viewPost;
    @SerializedName("like_post")
    @Expose
    private String likePost;
    @SerializedName("add_by")
    @Expose
    private String addBy;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("cat")
    @Expose
    private String cat;
    @SerializedName("text_music")
    @Expose
    private String text_music;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUnic() {
        return idUnic;
    }

    public void setIdUnic(String idUnic) {
        this.idUnic = idUnic;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

    public String getViewPost() {
        return viewPost;
    }

    public void setViewPost(String viewPost) {
        this.viewPost = viewPost;
    }

    public String getLikePost() {
        return likePost;
    }

    public void setLikePost(String likePost) {
        this.likePost = likePost;
    }

    public String getAddBy() {
        return addBy;
    }

    public void setAddBy(String addBy) {
        this.addBy = addBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getText_music() {
        return text_music;
    }

    public void setText_music(String text_music) {
        this.text_music = text_music;
    }
}