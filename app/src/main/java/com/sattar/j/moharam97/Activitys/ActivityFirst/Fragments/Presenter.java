package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments;



import com.sattar.j.moharam97.Contracts.FragmentsCateContract;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

public class Presenter implements FragmentsCateContract.Presenter {
    private FragmentsCateContract.View view;
    private Model model = new Model();


    public Presenter() {
        model.attachPresenter(this);
    }



    @Override
    public void attachView(FragmentsCateContract.View view) {
        this.view = view;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        model.getItems(musicPage, musicCat);
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        view.onListSuccessItems(music,cat);
    }

    @Override
    public void onListFailedItems(String msg) {
        view.musicPojoonListFailedItems(msg);
    }

}


