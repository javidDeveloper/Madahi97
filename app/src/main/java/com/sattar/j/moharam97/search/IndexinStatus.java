package com.sattar.j.moharam97.search;

/**
 * Created by mansour on 2/18/18.
 */

public enum IndexinStatus {
    NotIndexed, Indexing, Indexed
}