package com.sattar.j.moharam97.Contracts;

import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

/**
 * Created by javid on 10/18/18.
 */

public interface MoreActivityContract {
    interface View {
        void onListSuccessItems(List<MusicPojo> music, String cat);
        void onListFailedItems(String msg);
    }

    interface Presenter {
        void attachView(View view);
        void getItems(int musicPage, String musicCat);
        void onListSuccessItems(List<MusicPojo> music,String cat);
        void onListFailedItems(String msg);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void getItems(int musicPage, String musicCat);
        void onListSuccessItems(List<MusicPojo> music,String cat);
        void onListFailedItems(String msg);
    }
}
