package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.HomeRecycelerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.CateAllFragment;
import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by javid on 10/1/18.
 */

public class ParentRecyclerViewAdapter extends RecyclerView.Adapter<ParentRecyclerViewAdapter.ViewHolder> implements android.view.View.OnClickListener {
    private Context context;
    private List<RecyclerView> recyclerViewList;
    private android.view.View rootView;
    private List<BannerPojo> pojoHeader;
    private List<MusicPojo> newList = new ArrayList<>();
    private List<MusicPojo> viewList = new ArrayList<>();
    private List<MusicPojo> likeList = new ArrayList<>();
    private List<MusicPojo> pojoMusicList;

    public ParentRecyclerViewAdapter(Context context, List<RecyclerView> pojo) {
        this.context = context;
        this.recyclerViewList = pojo;
    }

    @NonNull
    @Override
    public ParentRecyclerViewAdapter.ViewHolder onCreateViewHolder
            (@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(context)
                .inflate(R.layout.recycler_parent_items, parent, false);
        ViewHolder holder = new ViewHolder(rootView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewAdapter.ViewHolder
                                         holder, int position) {

        /**************** header ******************/
        BannerRecyclerViewAdapter bannerAdapter = new BannerRecyclerViewAdapter(context, pojoHeader);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context,
                LinearLayout.HORIZONTAL, true);
        holder.headerRec.setLayoutManager(layoutManager);
        holder.headerRec.setAdapter(bannerAdapter);
        /**************** cat new of body ******************/
        MusicRecyclerViewAdapter newAdapter =
                new MusicRecyclerViewAdapter(context, newList);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(context,
                LinearLayout.HORIZONTAL, true);
        holder.cat1Rec.setLayoutManager(layoutManager1);
        holder.cat1Rec.setAdapter(newAdapter);
        holder.moreNew.setOnClickListener(this);

        /**************** cat view of body ******************/
        MusicRecyclerViewAdapter viewAdapter =
                new MusicRecyclerViewAdapter(context, viewList);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(context,
                LinearLayout.HORIZONTAL, true);
        holder.cat2Rec.setLayoutManager(layoutManager2);
        holder.cat2Rec.setAdapter(viewAdapter);
        holder.moreView.setOnClickListener(this);

        /**************** cat like of body ******************/
        MusicRecyclerViewAdapter likeAdapter =
                new MusicRecyclerViewAdapter(context, likeList);
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(context,
                LinearLayout.HORIZONTAL, true);
        holder.cat3Rec.setLayoutManager(layoutManager3);
        holder.cat3Rec.setAdapter(likeAdapter);
        holder.moreLike.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        int size = recyclerViewList.size();
        return size == 0 ? 0 : 1;
    }

    @Override
    public void onClick(android.view.View view) {
//        int id = view.getId();
//        switch (id) {
//            case R.id.moreNew:
//                Utility.goToActivityWithKey(context, MoreActivity.class, FirstActivity.CATE);
//                break;
//            case R.id.moreView:
//                Utility.goToActivityWithKey(context, MoreActivity.class, FirstActivity.CATE_VIEW);
//                break;
//            case R.id.moreLike:
//                Utility.goToActivityWithKey(context, MoreActivity.class, FirstActivity.CATE_LIKE);
//                break;
//            default:
//                break;
//        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView headerRec;
        RecyclerView cat1Rec;
        RecyclerView cat2Rec;
        RecyclerView cat3Rec;
        MyTextView moreNew;
        MyTextView moreView;
        MyTextView moreLike;

        public ViewHolder(android.view.View itemView) {
            super(itemView);
            headerRec = itemView.findViewById(R.id.headerRec);
            cat1Rec = itemView.findViewById(R.id.cat1Rec);
            cat2Rec = itemView.findViewById(R.id.cat2Rec);
            cat3Rec = itemView.findViewById(R.id.cat3Rec);
            moreNew = itemView.findViewById(R.id.moreNew);
            moreView = itemView.findViewById(R.id.moreView);
            moreLike = itemView.findViewById(R.id.moreLike);

        }
    }

    public void setPojoHeader(List<BannerPojo> pojoHeader) {
        this.pojoHeader = pojoHeader;
    }

    public void setPojoMusicList(List<MusicPojo> pojoMusicList) {
        this.pojoMusicList = pojoMusicList;
        int i = 0;
        int j = 0;
        int z = 0;
        for (MusicPojo musicPojo :pojoMusicList) {
            if (musicPojo.getCat().equals(CateAllFragment.CATE_NEW)) {
//                if (newList.size() > 0 && newList.size() > 0) {
//                    for (MusicPojo pojo : newList) {
//                        if (!pojo.getId().equals(newList.get(i).getId())) {
//                            newList.add(pojo);
//                            i++;
//                        }
//                    }
//                }
                newList.add(musicPojo);
            } else if (musicPojo.getCat().equals(CateAllFragment.CATE_VIEW)) {
//                if (viewList.size() > 0 && viewList.size() > 0) {
//                    for (MusicPojo pojo1 : viewList) {
//                        viewList.add(pojo1);
//                        if (!pojo1.getId().equals(viewList.get(j).getId())) {
//                            viewList.add(pojo1);
//                            j++;
//                        }
//                    }
//                }
                viewList.add(musicPojo);
            } else if (musicPojo.getCat().equals(CateAllFragment.CATE_LIKE)) {
//                if (likeList.size() > 0 && likeList.size() > 0) {
//                    for (MusicPojo pojo2 : likeList) {
//                        if (!pojo2.getId().equals(likeList.get(z).getId())) {
//                            likeList.add(pojo2);
//                            z++;
//                        }
//                    }
//                }
                likeList.add(musicPojo);
            }

        }
    }
}

