package com.sattar.j.moharam97.Services;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.MusicPlayerActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;

/**
 * Created by javid on 10/19/18.
 */

public class MusicPlayer extends Service {
    public static String STATUS_MUSIC = "status.music";
    public static String SEEK_MUSIC = "seek.music";
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private MusicPojo musicPojo;
    private String controll = "";
    private View view;
    private Intent intent;

    public MusicPojo getMusicPojo() {
        return musicPojo;
    }

    public void setMusicPojo(MusicPojo musicPojo) {
        this.musicPojo = musicPojo;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Serializable pojo = intent.getSerializableExtra(MusicPlayerActivity.PLAY_MUSIC);
            if (pojo instanceof MusicPojo) {
                setMusicPojo((MusicPojo) pojo);
                if (getMusicPojo() != null) {
                    playMusic();
                }
            }
            controll = intent.getStringExtra(STATUS_MUSIC);
            if (controll instanceof String) {
                if (mediaPlayer != null) {

                    if (controll.equals("pause")) {
                        mediaPlayer.pause();
                    }
                    if (controll.equals("play")) {
                        mediaPlayer.start();
                    }
                }
            }

        } catch (Exception e) {
            Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.release();
    }

    private void playMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(getMusicPojo().getLink());
                mediaPlayer.prepare();
                mediaPlayer.start();
                eventTimer();
            } catch (Exception e) {
                Toast.makeText(this, "error in playMusic() \n if (mediaPlayer != null)", Toast.LENGTH_SHORT).show();
            }

        } else {
            try {
                mediaPlayer.reset();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(getMusicPojo().getLink());
//                mediaPlayer.setDataSource("http://soundbible.com/grab.php?id=2026&type=mp3");
                mediaPlayer.prepare();
                mediaPlayer.start();
                eventTimer();
            } catch (Exception e) {
                Toast.makeText(this, "error in playMusic() \n } else {", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void eventTimer() {
        if (mediaPlayer != null) {
            EventBus.getDefault().post(mediaPlayer);
            EventBus.getDefault().post(getMusicPojo());
            EventBus.getDefault().post(musicPojo);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void controll(MediaPlayer player) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        } else {
            mediaPlayer.start();
        }
    }

}
