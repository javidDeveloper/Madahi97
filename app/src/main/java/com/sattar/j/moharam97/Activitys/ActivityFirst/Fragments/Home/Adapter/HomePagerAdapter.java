package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.CateAllFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.LikesFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.NewsFragment;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.ViewsFragment;

/**
 * Created by javid on 10/25/18.
 */

public class HomePagerAdapter extends FragmentPagerAdapter {
    public HomePagerAdapter(FragmentManager fragmentManager, int context) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CateAllFragment.newInstance();
            case 1:
                return NewsFragment.newInstance();
            case 2:
                return ViewsFragment.newInstance();
            case 3:
                return LikesFragment.newInstance();
            default:
                return null;
        }
    }

}
