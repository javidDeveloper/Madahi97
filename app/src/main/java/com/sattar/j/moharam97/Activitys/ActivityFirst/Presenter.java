package com.sattar.j.moharam97.Activitys.ActivityFirst;

import com.sattar.j.moharam97.Contracts.FirstActivityContract;
import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.List;

public class Presenter implements FirstActivityContract.Presenter {
    private FirstActivityContract.View view;
    private Model model = new Model();


    public Presenter() {
        model.attachPresenter(this);
    }


    @Override
    public void attachView(FirstActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        model.getItems(musicPage, musicCat);
    }

    @Override
    public void getHeaders(int bannerLimit) {
        model.getHeaders(bannerLimit);
    }

    @Override
    public void onListSuccessItems(List<BannerPojo> bannerPojos, List<MusicPojo> music, String cat) {
        view.onListSuccessItems(bannerPojos, music, cat);
    }


    @Override
    public void onListFailedItems(String msg) {
        view.onListFailedItems(msg);
    }

}


