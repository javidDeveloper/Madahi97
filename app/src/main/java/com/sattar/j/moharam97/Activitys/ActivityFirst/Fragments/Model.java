package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments;

import com.sattar.j.moharam97.Contracts.FragmentsCateContract;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Models.Retrofit.GetRest;

import java.util.List;

public class Model implements FragmentsCateContract.Model {
    private FragmentsCateContract.Presenter presenter;
    private GetRest rest = new GetRest();

    public Model() {
        rest.attachmusicFragmentCate(this);
    }

    @Override
    public void attachPresenter(FragmentsCateContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        rest.getCateMusic(musicPage, musicCat);
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        presenter.onListSuccessItems(music, cat);
    }


    @Override
    public void onListFailedItems(String msg) {
        presenter.onListFailedItems(msg);
    }


}
